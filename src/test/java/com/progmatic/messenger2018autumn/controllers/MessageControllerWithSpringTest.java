/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.messenger2018autumn.controllers;

import com.progmatic.messenger2018autumn.domain.Message;
import com.progmatic.messenger2018autumn.domain.User;
import com.progmatic.messenger2018autumn.services.MessageService;
import com.progmatic.messenger2018autumn.sessionbeans.UserInfo;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

/**
 *
 * @author peti
 */
@RunWith(SpringRunner.class)
@ContextConfiguration
public class MessageControllerWithSpringTest {

    private List<Message> mockMessages = new ArrayList<>();
    MessageService mockService;

    public MessageControllerWithSpringTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        mockMessages.add(new Message(new Long(1), new User("peti"), "hello"));
        mockMessages.add(new Message(new Long(2), new User("peti"), "szia"));

        mockService = Mockito.mock(MessageService.class);
        Mockito.when(mockService.findMessages(Mockito.any())).thenReturn(mockMessages);
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of createMessage method, of class MessageController.
     */
    @Test
    @WithMockUser(username = "pityuka")
    public void testCreateMessage() throws Exception {
        UserInfo ui = Mockito.mock(UserInfo.class);
        MockMvc mockMvc = MockMvcBuilders.standaloneSetup(new MessageController(ui, mockService))
                .setViewResolvers(new InternalResourceViewResolver("/ize", ".bigyo"))
                .build();
        mockMvc.perform(MockMvcRequestBuilders.post("/createmessage")
                .param("text", "hello"))
                .andExpect(MockMvcResultMatchers.redirectedUrl("/messagetable?orderBy=createDate&isAsc=desc"));
        Mockito.verify(mockService, Mockito.times(1)).createMessage(Mockito.any());

        mockMvc.perform(MockMvcRequestBuilders.post("/createmessage"))
                .andExpect(MockMvcResultMatchers.view().name("createMessage"));

    }

}
