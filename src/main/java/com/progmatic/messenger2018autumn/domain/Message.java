/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.messenger2018autumn.domain;

import com.progmatic.messenger2018autumn.utils.DateConstants;
import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.validation.constraints.NotBlank;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author peti
 */
@Entity
public class Message implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    private User author;

    @NotBlank
    @Lob
    private String text;

    @DateTimeFormat(pattern = DateConstants.DATETIME_PATTERN)
    private LocalDateTime createDate;

    @ManyToOne
    private Topic topic;

    @PrePersist
    public void init() {
        setCreateDate(LocalDateTime.now());
    }

    public Message(Long id, User author, String text) {
        this.id = id;
        this.author = author;
        this.text = text;
    }

    public Message(User author, String text, Topic topic) {
        this.author = author;
        this.text = text;
        this.topic = topic;
    }

    public Message() {
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public LocalDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Topic getTopic() {
        return topic;
    }

    public void setTopic(Topic topic) {
        this.topic = topic;
    }

    @Override
    public String toString() {
        return "Message{" + "id=" + id + ", author=" + author + ", text=" + text + ", createDate=" + createDate + ", topic=" + topic + '}';
    }

}
