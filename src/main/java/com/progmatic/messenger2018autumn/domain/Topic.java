/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.messenger2018autumn.domain;

import java.time.LocalDateTime;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedEntityGraphs;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;

/**
 *
 * @author peti
 */
@Entity
@NamedEntityGraphs({
    @NamedEntityGraph(
            name = "topicWithMessages",
            attributeNodes = {
                @NamedAttributeNode(value = "messages")
            })
})
public class Topic {

    @Id
    private String name;

    @ManyToOne
    private User author;

    private LocalDateTime createDate;

    @OneToMany(mappedBy = "topic", fetch = FetchType.LAZY)
    private List<Message> messages;

    public Topic() {
    }

    public Topic(String name, User author) {
        this.name = name;
        this.author = author;
    }
    

    public Topic(String name) {
        this.name = name;
    }
    
    @PrePersist
    public void init(){
        this.createDate = LocalDateTime.now();
    }
    
    

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public LocalDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }

}
