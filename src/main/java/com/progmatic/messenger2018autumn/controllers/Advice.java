/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.messenger2018autumn.controllers;

import com.progmatic.messenger2018autumn.sessionbeans.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

/**
 *
 * @author peti
 */
@ControllerAdvice
public class Advice {

    @ModelAttribute
    public void addAttributes(Model model) {
        model.addAttribute("welcomeMsg", "Hello here!");
    }

}
