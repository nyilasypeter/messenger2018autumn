/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.messenger2018autumn.controllers;

import com.progmatic.messenger2018autumn.domain.Message;
import com.progmatic.messenger2018autumn.services.MessageService;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author peti
 */
@RestController
public class MessageRestController {

    private static final Logger LOG = LoggerFactory.getLogger(MessageRestController.class);

    MessageService messageService;

    @Autowired
    public MessageRestController(MessageService messageService) {
        this.messageService = messageService;
    }

    @RequestMapping(path = "/delete/{id}", method = RequestMethod.POST)
    public Boolean deleteMessage(@PathVariable(name = "id") Long id) {
        return messageService.deleteMessage(id);
    }

    @RequestMapping(path = "/createDefaultMessage", method = RequestMethod.POST)
    public Message createMessage() {
        Message m = new Message();
        m.setText("Alapértelmezett");
        return messageService.createMessage(m);
    }

    @RequestMapping(path = "/findMessage/{id}", method = RequestMethod.GET)
    public Message findMessage(@PathVariable(name = "id") Long id) {
        return messageService.findMessageById(id);
    }

    @GetMapping(path = "/csrftoken")
    public String getCsrfToken(HttpServletRequest request, HttpServletResponse response) {
        HttpSessionCsrfTokenRepository repo = new HttpSessionCsrfTokenRepository();
        CsrfToken csrf = repo.loadToken(request);
        if(csrf == null){
            
            csrf = repo.generateToken(request);
            repo.saveToken(csrf, request, response);
        }
        return csrf.getToken();

    }

}
