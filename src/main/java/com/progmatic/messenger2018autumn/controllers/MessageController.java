/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.messenger2018autumn.controllers;

import com.progmatic.messenger2018autumn.aop.Loggable;
import com.progmatic.messenger2018autumn.domain.Message;
import com.progmatic.messenger2018autumn.domain.Topic;
import com.progmatic.messenger2018autumn.dto.MessageDTO;
import com.progmatic.messenger2018autumn.dto.MessageFilterDTO;
import com.progmatic.messenger2018autumn.services.MessageService;
import com.progmatic.messenger2018autumn.sessionbeans.UserInfo;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.util.HtmlUtils;

/**
 *
 * @author peti
 */
@Controller
@Loggable
public class MessageController {

    //private final ApplicationContext context;
    private final MessageService messageService;

    private final UserInfo ui;
    
    private static final Logger LOG = LoggerFactory.getLogger(MessageController.class);

    @Autowired
    public MessageController(UserInfo ui, MessageService messageService) {
        this.ui = ui;
        this.messageService = messageService;
    }

    @GetMapping(path = "proba")
    public String proba() {
        Topic t = messageService.findTopic("beer");
        List<Message> messages = t.getMessages();
        for (Message message : messages) {
            System.out.println("in controller: " + message);
        }
        return "home";
    }

    @GetMapping("/messagesoftopic")
    public String findMessagesByTopic(
            @RequestParam(name = "topicName") String topicName,
            Model model) {
        Topic topic = messageService.findTopicWithMessages(topicName);
        if (topic == null) {
            model.addAttribute("messages", new ArrayList<>());
        } else {
            model.addAttribute("messages", topic.getMessages());
        }
        return "messageList";

    }

    @RequestMapping(path = "/messagetable")
    public String showMessages(
            @ModelAttribute("messagefilter") MessageFilterDTO mdto,
            Model model) {
        model.addAttribute("messages", messageService.findMessages(mdto));
        return "messageList";
    }

    @RequestMapping(path = "/messages/{messageId}")
    public String showOneMessages(
            @PathVariable("messageId") Long msgId,
            Model model) {
        model.addAttribute("message", messageService.findMessageById(msgId));
        return "oneMessage";
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(path = "/messages/delete/{messageId}")
    public String deleteMessages(@PathVariable("messageId") Long msgId) {
        messageService.deleteMessage(msgId);
        return "redirect:/messagetable";
    }

    @GetMapping(path = "/showcreate")
    public String showCreateMessage(@ModelAttribute("messg") Message m,
            Model model) {
        model.addAttribute("topics", messageService.findAllTopicNames());
        return "createMessage";
    }

    @PostMapping(path = "/createmessage")
    public String createMessage(@Valid @ModelAttribute("messg") Message m, BindingResult br, Model model) {
        if (br.hasErrors()) {
            return "createMessage";
        }
        //UserInfo ui = context.getBean(UserInfo.class);

        messageService.createMessage(m);
        return "redirect:/messagetable?orderBy=createDate&isAsc=desc";
    }

    @MessageMapping("/acceptmessage")
    @SendTo("/topic/messages")
    public MessageDTO greeting(MessageDTO message) throws Exception {
        String userName = SecurityContextHolder.getContext().getAuthentication().getName();
        message.setAuthorName(userName);
        return message;
    }

    @GetMapping(path = "modifymessage")
    public String modifyMessage(
            @RequestParam(name = "id") Long messageId,
            @RequestParam(name = "text", required = false) String newText) {
        messageService.modifyMessage(messageId, newText);
        return "success";
    }
}
