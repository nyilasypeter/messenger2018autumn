/*
 * To change this license header; choose License Headers in Project Properties.
 * To change this template file; choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.messenger2018autumn.dto;

import com.progmatic.messenger2018autumn.utils.DateConstants;
import java.time.LocalDate;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author peti
 */
public class MessageFilterDTO {

    private Long id;
    
    private String author;
    
    private String text;
    
    private String topic;
    
    @DateTimeFormat(pattern = DateConstants.DATE_PATTERN)
    private LocalDate from;
    
    @DateTimeFormat(pattern = DateConstants.DATE_PATTERN)
    private LocalDate to;
    
    private String orderBy;
    
    private String isAsc;
    
    private Integer limit;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public LocalDate getFrom() {
        return from;
    }

    public void setFrom(LocalDate from) {
        this.from = from;
    }

    public LocalDate getTo() {
        return to;
    }

    public void setTo(LocalDate to) {
        this.to = to;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    public String getIsAsc() {
        return isAsc;
    }

    public void setIsAsc(String isAsc) {
        this.isAsc = isAsc;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    @Override
    public String toString() {
        return "MessageFilterDTO{" + "id=" + id + ", author=" + author + ", text=" + text + ", topic=" + topic + ", from=" + from + ", to=" + to + ", orderBy=" + orderBy + ", isAsc=" + isAsc + ", limit=" + limit + '}';
    }
    
    
    
    
}
