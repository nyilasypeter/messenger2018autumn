/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.messenger2018autumn.services;

import com.progmatic.messenger2018autumn.domain.User;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author peti
 */
@Service
public class UserService implements UserDetailsService {

    @PersistenceContext
    EntityManager em;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Transactional
    public void createUser(String userName, String password) {
        User user = new User(userName, passwordEncoder.encode(password), "USER");
        em.persist(user);
    }

    @Transactional
    public void createUser(String userName, String password, String... auths) {
        User user = new User(userName, passwordEncoder.encode(password), auths);
        em.persist(user);
    }

    public boolean userExists(String name) {
        User u = em.find(User.class, name);
        return u != null;
    }

    @Override
    public User loadUserByUsername(String username) throws UsernameNotFoundException {
        try {
            User u = (User) em.createQuery("SELECT u  from User u  join fetch u.authorities au where u.username = :un")
                    .setParameter("un", username)
                    .getSingleResult();
            return u;
        } catch (NoResultException e) {
            throw new UsernameNotFoundException(username);
        }        
    }

}
