/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.messenger2018autumn.services;

import com.progmatic.messenger2018autumn.domain.Message;
import com.progmatic.messenger2018autumn.domain.Message_;
import com.progmatic.messenger2018autumn.domain.Topic;
import com.progmatic.messenger2018autumn.domain.Topic_;
import com.progmatic.messenger2018autumn.domain.User;
import com.progmatic.messenger2018autumn.domain.User_;
import com.progmatic.messenger2018autumn.dto.MessageFilterDTO;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

/**
 *
 * @author peti
 */
@Service
public class MessageService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MessageService.class);

    @PersistenceContext
    EntityManager em;

    @Autowired
    UserService userService;

    @Transactional
    public List<Message> findMessages(MessageFilterDTO filter) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Message> cQ = cb.createQuery(Message.class);
        Root<Message> from = cQ.from(Message.class);
        Join<Message, User> joinedAuthor = from.join(Message_.author);
        Join<Message, Topic> joinedTopic = from.join(Message_.topic);
        List<Predicate> restrictions = new ArrayList<>();
        if (filter.getId() != null) {
            restrictions.add(cb.equal(from.get(Message_.id), filter.getId()));
        }
        if (StringUtils.hasText(filter.getText())) {
            restrictions.add(cb.like(from.get(Message_.text), "%" + filter.getText() + "%"));
        }
        if (StringUtils.hasText(filter.getAuthor())) {
            restrictions.add(cb.equal(joinedAuthor.get(User_.username), filter.getAuthor()));
        }
        if (StringUtils.hasText(filter.getTopic())) {
            restrictions.add(cb.equal(joinedTopic.get(Topic_.name), filter.getTopic()));
        }
        if (filter.getFrom() != null) {
            Predicate greaterThan = cb.greaterThan(from.get(Message_.createDate), filter.getFrom().atStartOfDay());
            restrictions.add(greaterThan);
        }
        if (filter.getTo() != null) {
            Predicate lessThan = cb.lessThan(from.get(Message_.createDate), filter.getTo().atStartOfDay());
            restrictions.add(lessThan);
        }
        cQ.select(from).where(cb.and(restrictions.toArray(new Predicate[restrictions.size()])));
        if (StringUtils.hasText(filter.getOrderBy())) {
            Expression e = null;
            switch (filter.getOrderBy()) {
                case "text":
                    e = from.get(Message_.text);
                    break;
                case "author":
                    e = joinedAuthor.get(User_.username);
                    break;
                case "createDate":
                    e = from.get(Message_.createDate);
            }
            if("desc".equals(filter.getIsAsc())){
                cQ.orderBy(cb.desc(e));
            }
            else{
                cQ.orderBy(cb.asc(e));
            }
        }
        Query q;
        if (filter.getLimit() != null) {
            q = em.createQuery(cQ).setMaxResults(filter.getLimit());
        } else {
            q = em.createQuery(cQ);

        }
        return q.getResultList();
    }

    public Topic findTopic(String topicId) {
        Topic t = em.find(Topic.class,  topicId);
        return t;
    }

    public Topic findTopicWithMessages(String topicId) {
        EntityGraph<?> entityGraph = em.getEntityGraph("topicWithMessages");
        Map<String, Object> prop = new HashMap<>();
        prop.put("javax.persistence.loadgraph", entityGraph);
        Topic t = em.find(Topic.class, topicId, prop);
        //t.getMessages().forEach(m -> LOGGER.debug(m.getText()));
        return t;
    }

    @Transactional(isolation = Isolation.REPEATABLE_READ)
    public Message findMessageById(Long id) {
        Message m = em.find(Message.class, id);
        return m;
    }

    private void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException ex) {
            throw new RuntimeException(ex);
        }
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @Transactional
    public boolean deleteMessage(Long id) {
        Message m = em.find(Message.class, id);
        if (m == null) {
            return false;
        }
        em.remove(m);
        return true;
    }

    @Transactional
    public Message createMessage(Message m) {
        UserDetails user = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User myUser = new User();
        myUser.setUsername(user.getUsername());
        m.setAuthor(myUser);
        em.persist(m);
        return m;
    }

    @Transactional(isolation = Isolation.REPEATABLE_READ)
    public void modifyMessage(Long id, String idText) {
        LOGGER.debug("modifyMessage started. text: {}", idText);
        Message m = em.find(Message.class, id, LockModeType.PESSIMISTIC_READ);
        LOGGER.debug("modifyMessage ({}) found message (gonna sleep now)", idText);
        sleep(10_000);
        m.setText(m.getText() + "_a");
        LOGGER.debug("modifyMessage ({}) modified message (finished to sleep)", idText);
        //sleep(10_000);
    }

    public List<String> findAllTopicNames() {
        return em.createQuery("select t.name from Topic t ").getResultList();
    }
}
