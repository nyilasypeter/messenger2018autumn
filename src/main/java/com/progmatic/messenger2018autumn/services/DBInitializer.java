/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.messenger2018autumn.services;

import com.progmatic.messenger2018autumn.domain.Authority;
import com.progmatic.messenger2018autumn.domain.Message;
import com.progmatic.messenger2018autumn.domain.Topic;
import com.progmatic.messenger2018autumn.domain.User;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.ServletException;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 *
 * @author peti
 */
@Service
public class DBInitializer {

    @PersistenceContext
    EntityManager em;

    @Autowired
    UserService userService;

    @Transactional
    public void init() {
        createRoles();
        createUsers();
        em.flush();
        createTopics();
        em.flush();
        createMessage();
    }

    private void createRoles() {
        List<Authority> auths = em.createQuery("select a from Authority a").getResultList();
        if (auths.isEmpty()) {
            Authority user = new Authority();
            user.setName("USER");
            em.persist(user);

            Authority admin = new Authority();
            admin.setName("ADMIN");
            em.persist(admin);
        }
    }

    private void createUsers() {
        List<User> users = em.createQuery("select u from User u").getResultList();
        if (users.isEmpty()) {
            userService.createUser("admin", "admin", "ADMIN");
            authors.forEach(userName -> userService.createUser(userName, userName));
        }

    }

    private void createTopics() {
        List<Topic> topicsInDb = em.createQuery("select t from Topic t").getResultList();
        if (topicsInDb.isEmpty()) {
            topics.forEach(topic -> {
                topic.setAuthor(new User(randomAuthor()));
                em.persist(topic);
            });
        }
    }

    private void createMessage() {
        List<Message> messages = em.createQuery("select m from Message m").getResultList();
        if (messages.isEmpty()) {
            Topic t = new Topic();
            t.setName("irodalom");
            LongStream.range(1, 100)
                    .mapToObj(i -> {
                        Message m = new Message(new User(randomAuthor()), randomText(), randomTopic());
                        return m;
                    })
                    .forEach(m -> em.persist(m));
        }
    }

    private static final List<String> authors = new ArrayList<>();
    private static final List<Topic> topics = new ArrayList<>();
    private static final List<String> blabla = new ArrayList<>();
    private static final List<String> signs = new ArrayList<>();
    private static final Random r = new Random();

    static {
        authors.add("peti");
        authors.add("Mézga Aladár");
        authors.add("Mézga Kriszta");
        authors.add("Mézga Géza");
        authors.add("Mézgánél szül. rezovics Paula");
        authors.add("Dr Máris");
        authors.add("MZ/X");

        blabla.add("lorem");
        blabla.add("ipsum");
        blabla.add("doloret");
        blabla.add("umque");
        blabla.add("populus");
        blabla.add("usque");
        blabla.add("tandem");
        blabla.add("abutere");
        blabla.add("patienta");

        signs.add(".");
        signs.add("!");
        signs.add("?");
        
        topics.add(new Topic("irodalom"));
        topics.add(new Topic("sör"));
        topics.add(new Topic("tudomány"));

    }

    private static String randomAuthor() {
        return authors.get(r.nextInt(authors.size()));
    }
    
    private static Topic randomTopic() {
        return topics.get(r.nextInt(topics.size()));
    }

    private static String randomText() {
        int length = r.nextInt(20) + 2;
        return IntStream.range(0, length)
                .mapToObj(i -> randomSentence())
                .collect(Collectors.joining(" "));
    }

    private static String randomSentence() {
        int length = r.nextInt(8) + 3;
        String sentence = IntStream.range(0, length)
                .mapToObj(i -> blabla.get(r.nextInt(blabla.size())))
                .collect(Collectors.joining(" ", "", signs.get(r.nextInt(signs.size()))));
        return StringUtils.capitalize(sentence);
    }

    @EventListener(ContextRefreshedEvent.class)
    public void onAppStartup(ContextRefreshedEvent ev) throws ServletException {
        DBInitializer me = ev.getApplicationContext().getBean(DBInitializer.class);
        me.init();
    }

}
