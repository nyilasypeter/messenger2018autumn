/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.messenger2018autumn.helpers;

/**
 *
 * @author peti
 */
public enum JPQLParamCondition {
    EQUALS("="), LIKE("LIKE"), LT("<"), GT(">"), LTE("<="), GTE(">=");
    
    private final String condString;

    private JPQLParamCondition(String condString) {
        this.condString = condString;
    }
    
    public String asString(){
        return condString;
    }
    
}
