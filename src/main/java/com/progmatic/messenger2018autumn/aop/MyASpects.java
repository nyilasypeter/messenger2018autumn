/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.messenger2018autumn.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author peti
 */
@Aspect
@Configuration
public class MyASpects {

    private static Logger LOGGER = LoggerFactory.getLogger(MyASpects.class);

    @Before("@annotation(com.progmatic.messenger2018autumn.aop.Loggable) || @within(com.progmatic.messenger2018autumn.aop.Loggable)")
    public void doLog(JoinPoint jp) {
        methodStarted(jp);
    }

    @AfterReturning(value = "@annotation(com.progmatic.messenger2018autumn.aop.Loggable) || @within(com.progmatic.messenger2018autumn.aop.Loggable)", returning = "returnValue")
    public void doLogAfter(JoinPoint jp, Object returnValue) {
        methodFinished(jp, returnValue);
    }


    private void methodStarted(JoinPoint jp) {
        LOGGER.trace("method {} of class {} started. Arguments: {}",
                jp.getSignature().getName(),
                jp.getSignature().getDeclaringTypeName(),
                jp.getArgs());
    }

    private void methodFinished(JoinPoint jp, Object returnValue) {
        LOGGER.trace("method {} of class {} finsihed. Return value is: {}",
                jp.getSignature().getName(),
                jp.getSignature().getDeclaringTypeName(),
                returnValue);
    }

}
