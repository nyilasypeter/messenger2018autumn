/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var stompClient = null;


function connect() {
    var socket = new SockJS('/messenger-ws-endpoint');
    stompClient = Stomp.over(socket);
    $.get("csrftoken", function (data) {
        var headers = {"X-CSRF-TOKEN": data};
        stompClient.connect(headers, function (frame) {
            console.log('Connected: ' + frame);
            stompClient.subscribe('/topic/messages', function (greeting) {
                showGreeting(JSON.parse(greeting.body));
            });
        });
    });

}

function disconnect() {
    try {
        if (stompClient !== null) {
            stompClient.disconnect();
        }
        console.log("Disconnected");
    } 
    catch (err) {
        console.log("Error during disconnect: " + err)
    }

}


function showGreeting(message) {
    alert(message.text + " by:" + message.authorName);
}



$(document).ready(function () {

    if ($("#needSockJs").length) {
        connect();
        window.onbeforeunload = function (event) {

            disconnect();
            console.log("websocket disconnect done");
        }
        return "";
    }
    
    $("#bcDiv").click(function () {
        var myDiv = $(this);
        myDiv.text(myDiv.text() + "_" + myDiv.text())
    });

    $(".msgListTableRow:even").css("background-color", "#E1D7D7");

    $("#hideShowButton").click(function () {
        $("#searchMessagesForm").toggle();
        var link = $(this);
        if (link.text() === "Hide") {
            link.text("Show");
        } else {
            link.text("Hide");
        }
    });


    /*$.get("/csrftoken", function (data, success) {
     $.post("/createmessage",
     {
     "topic.name": "irodalom",
     "text": "Szia (js-bol)",
     "_csrf": data
     },
     function (data, success) {
     alert(success);
     });
     });*/

    $("#createNewMessageButton").click(function (event) {
        event.preventDefault();
        stompClient.send("/app/acceptmessage", {}, JSON.stringify({
            'text': $("#text").val(),
            'author': 'peti'
        }));
    });

});


